# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as jellyfin_docker with context %}

{%- set mountpoint = jellyfin_docker.docker.mountpoint %}

jellyfin-docker-config-clean-config-directory-moved:
  file.rename:
    - name: {{ mountpoint }}/docker/jellyfin_cleaned
    - source: {{ mountpoint }}/docker/jellyfin
