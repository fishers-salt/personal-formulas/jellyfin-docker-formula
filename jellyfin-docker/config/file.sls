# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_container_running = tplroot ~ '.container.running' %}
{%- from tplroot ~ "/map.jinja" import mapdata as jellyfin_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_container_running }}

{%- set mountpoint = jellyfin_docker.docker.mountpoint %}
{%- set user = jellyfin_docker.owner.user %}
{%- set group = jellyfin_docker.owner.group %}

{%- for dir in [
  'docker/jellyfin/config',
  'shares/media/tv',
  'shares/media/movies',
  'shares/audio/audiobooks',
  'shares/audio/music'
  ]
%}
jellyfin-docker-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

jellyfin-docker-config-file-jellyfin-max-user-watches-configured:
  sysctl.present:
    - name: fs.inotify.max_user_watches
    - value: {{ jellyfin_docker.max_user_watches }}
