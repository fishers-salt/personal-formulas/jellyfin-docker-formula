# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as jellyfin_docker with context %}

include:
  - {{ sls_config_clean }}

{%- set name = jellyfin_docker.container.name %}
{%- set image = jellyfin_docker.container.image %}

jellyfin-docker-container-clean-container-absent:
  docker_container.absent:
    - name: {{ name }}
    - force: True

jellyfin-docker-docker-clean-image-absent:
  docker_image.absent:
    - name: {{ image }}
    - require:
      - jellyfin-docker-container-clean-container-absent
