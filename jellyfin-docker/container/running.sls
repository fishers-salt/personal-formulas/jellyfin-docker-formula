# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as jellyfin_docker with context %}

{%- set mountpoint = jellyfin_docker.docker.mountpoint %}

jellyfin-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/jellyfin
    - makedirs: True

jellyfin-docker-image-present:
  docker_image.present:
    - name: {{ jellyfin_docker.container.image }}
    - tag: {{ jellyfin_docker.container.tag }}
    - force: True

jellyfin-docker-container-running:
  docker_container.running:
    - name: {{ jellyfin_docker.container.name }}
    - image: {{ jellyfin_docker.container.image }}:{{ jellyfin_docker.container.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/jellyfin/config:/config
      - {{ mountpoint }}/shares/media/tv:/data/tvshows
      - {{ mountpoint }}/shares/media/movies:/data/movies
      - {{ mountpoint }}/shares/audio/audiobooks:/data/audiobooks
      - {{ mountpoint }}/shares/audio/music:/data/music
    - port_bindings:
      - 8197:8096
    - dns: {{ jellyfin_docker.dns_servers }}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.jellyfin-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.jellyfin-web.middlewares=jellyfin-redirect-websecure"
      - "traefik.http.routers.jellyfin-web.rule=Host(`{{ jellyfin_docker.container.url }}`)"
      - "traefik.http.routers.jellyfin-web.entrypoints=web"
      - "traefik.http.routers.jellyfin-websecure.rule=Host(`{{ jellyfin_docker.container.url }}`)"
      - "traefik.http.routers.jellyfin-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.jellyfin-websecure.tls=true"
      - "traefik.http.routers.jellyfin-websecure.entrypoints=websecure"
