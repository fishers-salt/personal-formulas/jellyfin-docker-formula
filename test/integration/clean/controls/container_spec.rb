# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'jellyfin' }
  its('images') { should_not include 'linuxserver/jellyfin:latest' }
end
