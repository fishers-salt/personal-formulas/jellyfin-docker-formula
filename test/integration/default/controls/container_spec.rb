# frozen_string_literal: true

describe docker_container(name: 'jellyfin') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'linuxserver/jellyfin:latest' }
  it { should have_volume('/config', '/srv/docker/jellyfin/config') }
  it { should have_volume('/data/tvshows', '/srv/shares/media/tv') }
  it { should have_volume('/data/movies', '/srv/shares/media/movies') }
  it { should have_volume('/data/audiobooks', '/srv/shares/audio/audiobooks') }
  it { should have_volume('/data/music', '/srv/shares/audio/music') }
end
